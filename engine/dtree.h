#include <iostream>
#include <iomanip>
#include <set>
#include <math.h>
#include <string>
#include <fstream>
#include<sstream>
#include <stdlib.h>
#include <map>

#define DISCOUNT 1
//#define DEPTH 2	// Depth of tree

using namespace std;

class state_t {
    int *splits;
    double *thresholds;
    int Count;

    int sizeOfArrays;
    int numTreeNodeVars;

  public:

    state_t()
    {
    }

    state_t(int DEPTH)
    {
        numTreeNodeVars = (int)(pow(2, DEPTH+1) - 1);
        sizeOfArrays = numTreeNodeVars/2;
            
        splits = new int[sizeOfArrays];
        thresholds = new double[sizeOfArrays];

        for (int i=0;i<sizeOfArrays;i++)
        {
            splits[i] = 0;
            thresholds[i] = 0;
        }

        Count = numTreeNodeVars/2;
    }

    state_t(const state_t &s)
    {
        sizeOfArrays = s.sizeOfArrays;

        splits = new int[sizeOfArrays];
        thresholds = new double[sizeOfArrays];

     	for(int i = 0; i < sizeOfArrays; i++)
        {
            splits[i] = s.splits[i];
            thresholds[i] = s.thresholds[i];
        }

     	Count = s.Count;
     	numTreeNodeVars = s.numTreeNodeVars;
    }

    ~state_t() {
	delete splits;
	delete thresholds;
     }

    size_t hash() const {
    	size_t num = 0;
    	for(int i = 0; i < sizeOfArrays - Count; i++)
        {
            num = num + (i+1)*(splits[i]+(int)(thresholds[i]*10)); 
        }
    	return num;
    }
   
    int Counter() const {
      return Count;
    }

  
    const state_t& operator=( const state_t &s)
    {
        sizeOfArrays = s.sizeOfArrays;

        splits = new int[sizeOfArrays];
        thresholds = new double[sizeOfArrays];

        for(int i = 0; i < sizeOfArrays; i++)
        {
            splits[i] = s.splits[i];
            thresholds[i] = s.thresholds[i];
        }

        Count = s.Count;
        numTreeNodeVars = s.numTreeNodeVars;

        return *this;
    }

    bool operator==(const state_t &s) const {
        if (sizeOfArrays!=s.sizeOfArrays)
            return false;

    	for(int i = 0; i < sizeOfArrays; i++)
    	{
    		if(splits[i] != s.splits[i])
    			return false;
            if (thresholds[i]!=s.thresholds[i])
                return false;
    	}

    	if(Count != s.Count)
    		return false;

    	if(numTreeNodeVars != s.numTreeNodeVars)
    		return false;

    	return true;
    }

    bool operator!=(const state_t &s) const {
        return !(*this==(s));   
    }

    bool operator<(const state_t &s) const {
        if (Count < s.Count)
            return false;
        if (Count > s.Count)
            return true;

    	return false;
    }


     void print(std::ostream &os) const {
        for (int i=0;i<numTreeNodeVars/2-Count;i++)
        {
            os<<"Node "<<i+1<<" : "<<splits[i]<<" , ";
            os<<setprecision(6)<<thresholds[i]<<"\n";
        }
        os<<"Num decisions remaining: "<<Count<<"\n";
             
    }
    friend class problem_t;
};

inline std::ostream& operator<<(std::ostream &os, const state_t &s) {
    s.print(os);
    return os;
}

class problem_t : public Problem::problem_t<state_t> {
  
    int DEPTH; 
    state_t init_;

    ifstream read;
    float **dataset;
    string *datasetfile;
    int numRows, numColumns;

    int *numDistinctVals;
    int numActions;

    float *interpretabilityWeights;
    float lambda;

    float penaltyCost;
 
    map<int, pair<int, float> > *actionMap; 
    

    vector<string> splitStr(string str, char delimiter) 
    {
        vector<string> internal;
        stringstream ss(str); // Turn the string into a stream.
        string tok;
  
        while(getline(ss, tok, delimiter)) {
           internal.push_back(tok);
        }
  
        return internal;
    }


  public:
    problem_t(int depth, int dim2, string filename, string interpretWeightString, float lambdaVal, float penalty)
      : Problem::problem_t<state_t>(DISCOUNT)
        {
            DEPTH = depth;
            state_t temp(DEPTH);
            init_ = temp;
            //datasetfile = new string("/Users/amulyayadav/Dropbox/Work/PostAAMAS/PhebeWork/Bottleneck/BinaryDatasets/german_numer/pres1/german_numer_normalized_pres1.csv");
            //datasetfile = new string("/Users/amulyayadav/Dropbox/Work/PostAAMAS/PhebeWork/Bottleneck/amulya_datasmall.csv");
            //datasetfile = new string("amulya_datasmall.csv");
            datasetfile = new string(filename.c_str());
            cout<<"Passed PArams: "<<DEPTH<<" "<<*datasetfile<<"\n";

            read.open(datasetfile->c_str());

            string dummy;
            read>>numRows>>numColumns>>dummy;
            cout<<"NumRows: "<<numRows<<"\n";
            cout<<"NumColumns: "<<numColumns<<"\n";

            dataset = new float*[numRows];
            for (int i=0;i<numRows;i++)
            {
                dataset[i] = new float[numColumns];
            }

            float a;
            for (int i=0;i<numRows;i++)
            {
                for (int j=0;j<numColumns;j++)
                {
                    //read>>a;
                    read>>dataset[i][j];
                }
            }

            penaltyCost = penalty;
            lambda = lambdaVal;
            interpretabilityWeights = new float[numColumns-1];
            vector<string> weights = splitStr(interpretWeightString,':');
            if (weights.size()!=numColumns-1)
            {
                 cout<<"SILLY GUY\n";
                 exit(0);
            }
            for (int i=0;i<numColumns-1;i++)
            {
                interpretabilityWeights[i] = atof(weights[i].c_str()); 
            }
            cout<<"Lambda: "<<lambda<<"\n";
	    cout<<"Interpretability weights: \n";
            for (int i=0;i<numColumns-1;i++)
            {
                cout<<setprecision(6)<<interpretabilityWeights[i]<<" ";
            }
            cout<<"\n";


            actionMap = new map<int, pair<int, float> >();

            numDistinctVals = new int[numColumns-1];///last feature is label
            map<float, int> distinctVal;
            int actionIndex=0;
            for (int j=0;j<numColumns-1;j++)
            {
                distinctVal.clear();
                for (int i=0;i<numRows;i++)
                {
                    distinctVal.insert(pair<float, int>(dataset[i][j], 1));
                }
                numDistinctVals[j] = distinctVal.size();

                for (map<float, int>::iterator it=distinctVal.begin();it!=distinctVal.end();it++)
                {
                    actionMap->insert(pair<int, pair<int, float> >(actionIndex++, pair<int, float>(j, it->first)));
                }
            }
            //read.close();

            numActions=actionMap->size();
        }

    virtual ~problem_t() { }

    virtual Problem::action_t number_actions(const state_t &s) const { return numActions; }

    virtual const state_t& init() const { return init_; }
    virtual bool terminal(const state_t &s) const {
        if(s.Count == -1)
        	return true;
        return false;
    }
    virtual bool dead_end(const state_t &s) const { return false; }
    virtual bool applicable(const state_t &s, ::Problem::action_t a) const {
        return true;
    }


    virtual float terminalCost(const state_t &s) const
    {
        cout<<"Inside terminal cost\n";
	cout<<"Original state:\n";
        s.print(std::cout);

        int numTreeNodeVars = (int)(pow(2, DEPTH+1) - 1);
        int count=s.Count;

        int splits[s.sizeOfArrays];
        double thresholds[s.sizeOfArrays];
        for (int i=0;i<numTreeNodeVars/2-count;i++)
        {
            splits[i] = s.splits[i];
            thresholds[i] = s.splits[i];
        }
        for (int n=numTreeNodeVars/2-count; n<numTreeNodeVars/2;n++)
        {
             int parent;
             if (n%2==0)
                 parent=n/2-1;
             else 
                 parent=n/2;

             splits[n] = s.splits[parent];
             thresholds[n] = s.splits[parent];
        }
	cout<<"Completed state: \n";
        for (int i=0;i<numTreeNodeVars/2;i++)
        {
            cout<<"Node "<<i+1<<" : "<<splits[i]<<" , ";
            cout<<setprecision(6)<<thresholds[i]<<"\n";
        }
        cout<<"Num decisions remaining: "<<count<<"\n";

        int leaves[numTreeNodeVars/2+1][2];
        for (int l=0;l<numTreeNodeVars/2+1; l++)
        {
            for (int j=0;j<2;j++)
                leaves[l][j]=0;
        }

        int currNode;

        for(int i = 0; i < numRows; i++)
        {
            currNode = 0;
            while(currNode < numTreeNodeVars/2)
            {
                float signAtNode = dataset[i][splits[currNode]] - thresholds[currNode];
                if (signAtNode <0)
                    currNode = 2*currNode+1;
                else if (signAtNode >=0)
                    currNode = 2*currNode+2;
            }

            if (dataset[i][numColumns-1]<0.001)
            {
                leaves[currNode-numTreeNodeVars/2][0]++;
            }
            else if (dataset[i][numColumns-1]>0.999)
            {
                leaves[currNode-numTreeNodeVars/2][1]++;
            }
        }

        float correctClassifications = numRows;
        for (int l=0;l<numTreeNodeVars/2+1;l++)
        {
            if (leaves[l][0] > leaves[l][1])
                correctClassifications -= leaves[l][1];
            else if (leaves[l][0] <= leaves[l][1])
                correctClassifications -= leaves[l][0];
        }



        return (-correctClassifications);
    }

    virtual float cost(const state_t &s, Problem::action_t a) const {
        int numTreeNodeVars = (int)(pow(2, DEPTH+1) - 1);
        
        if (s.Count > 0)    // Not a terminal state
        {
            pair<int, double> split = actionMap->find(a)->second; 
            int currNode = numTreeNodeVars/2 - s.Count;

            float numOccurrences=0;
            while(currNode >=0)
            {
                if (currNode%2==0)
                    currNode = currNode/2-1;
                else
                    currNode = currNode/2;
               
                if (currNode>=0)
                {
                    if (s.splits[currNode]==split.first)
                        numOccurrences=numOccurrences+1;
                }
            }
            
            return penaltyCost*numOccurrences;
            //return 0;
        }

                    

        int leaves[numTreeNodeVars/2+1][2]; 
        for (int l=0;l<numTreeNodeVars/2+1; l++)
        {
            for (int j=0;j<2;j++)
                leaves[l][j]=0;
        }

        //s.print(std::cout);

        int currNode;

        for(int i = 0; i < numRows; i++)
        {
            currNode = 0;
            while(currNode < numTreeNodeVars/2)
            {
		//cout<<currNode<<"\n";
                float signAtNode = dataset[i][s.splits[currNode]] - s.thresholds[currNode];
                if (signAtNode <0)
                    currNode = 2*currNode+1;
                else if (signAtNode >=0)
                    currNode = 2*currNode+2;
            }
	    
            if (dataset[i][numColumns-1]<0.001)
            {
                leaves[currNode-numTreeNodeVars/2][0]++;
            }
            else if (dataset[i][numColumns-1]>0.999)
            {
                leaves[currNode-numTreeNodeVars/2][1]++;
            }
        }

        float correctClassifications = numRows;
        for (int l=0;l<numTreeNodeVars/2+1;l++)
        {
            if (leaves[l][0] > leaves[l][1])
                correctClassifications -= leaves[l][1];
            else if (leaves[l][0] <= leaves[l][1])
                correctClassifications -= leaves[l][0];
        }
     
        float interpretComponent=0;
        for (int f=0;f<numColumns-1;f++)
        {
            bool isUsed=false;
            for (int i=0;i<numTreeNodeVars/2;i++)
            {
                if (s.splits[i]==f)
                {
                    isUsed = true;
                    break;
                }
            }
            interpretComponent += lambda*interpretabilityWeights[f];
        }  
        
        correctClassifications += interpretComponent;
         
        return (-correctClassifications);

    }
  

    // virtual float trans_prob(const state_t &s1, Problem::action_t a, const state_t &s2) const
    // {
    // 	// May not be required
    // }

    virtual void sample_factored(const state_t &s, Problem::action_t a, state_t &outcome) const 
    {
	if (s.Count==0)
	{
		outcome=s;
		outcome.Count--;
		return;
	}
        outcome = s;
        pair<int, double> split = actionMap->find(a)->second;
        outcome.splits[outcome.numTreeNodeVars/2-outcome.Count] = split.first;
        outcome.thresholds[outcome.numTreeNodeVars/2-outcome.Count] = split.second;

        outcome.Count = outcome.Count-1;
    }


    virtual void next(const state_t &s, Problem::action_t a, std::vector<std::pair<state_t,float> > &outcomes) const {
       
         
    }
    virtual void print(std::ostream &os) const { }
};

inline std::ostream& operator<<(std::ostream &os, const problem_t &p) {
    p.print(os);
    return os;
}

class scaled_heuristic_t : public Heuristic::heuristic_t<state_t> {
    const Heuristic::heuristic_t<state_t> *h_;
    float multiplier_;
  public:
    scaled_heuristic_t(const Heuristic::heuristic_t<state_t> *h, float multiplier = 1.0)
      : h_(h), multiplier_(multiplier) { }
    virtual ~scaled_heuristic_t() { }
    virtual float value(const state_t &s) const {
        return h_->value(s) * multiplier_;
    }
    virtual void reset_stats() const { }
    virtual float setup_time() const { return 0; }
    virtual float eval_time() const { return 0; }
    virtual size_t size() const { return 0; }
    virtual void dump(std::ostream &os) const { }
    float operator()(const state_t &s) const { return value(s); }
};

class zero_heuristic_t: public Heuristic::heuristic_t<state_t> {
  public:
    zero_heuristic_t() { }
    virtual ~zero_heuristic_t() { }
    virtual float value(const state_t &s) const { return 0; }
    virtual void reset_stats() const { }
    virtual float setup_time() const { return 0; }
    virtual float eval_time() const { return 0; }
    virtual size_t size() const { return 0; }
    virtual void dump(std::ostream &os) const { }
    float operator()(const state_t &s) const { return value(s); }
};


