#include <iostream>
#include <iomanip>
#include <strings.h>
#include <set>

#define DISCOUNT 1
#define NUM_NODES 10	// Number of nodes in network
#define MAX_TIME 6		// Total number of sessions
#define MAX_QUERIES 2	// Number of queries allowed per session
#define MAX_INVITE 3	// Maximum number of people to invite for the intervention
#define MAX_PER_QUERY 5	// Maximum number of nodes asked for in a query

class state_t {
    short* presence;
    std::set<int> invited;
    unsigned short queries_left;
    unsigned short time_index;

  public:
    state_t()
   {
        presence = new short[NUM_NODES];
        invited.clear();
        queries_left = -1;
        time_index = -1;
    }

    state_t(const state_t &s)
     {
     	presence = new short[NUM_NODES];
     	for(int i = 0; i < NUM_NODES; i++)
     		presence[i] = s.presence[i];

     	invited = s.invited;
     	queries_left = s.queries_left;
     	time_index = s.time_index;
      }

    ~state_t() { }

    size_t hash() const {
    	size_t num = 0;
    	for(int i = 0; i < NUM_NODES; i++)
    		num += (presence[i]+1);
    	num += queries_left*10000;
    	num += time_index*100;
    	return num;
    }

  
    const state_t& operator=( const state_t &s)
    {
     	presence = new short[NUM_NODES];
     	for(int i = 0; i < NUM_NODES; i++)
     		presence[i] = s.presence[i];

     	invited = s.invited;
     	queries_left = s.queries_left;
     	time_index = s.time_index;

        return *this;
    }

    bool operator==(const state_t &s) const {
    	for(int i = 0; i < NUM_NODES; i++)
    	{
    		if(presence[i] != s.presence[i])
    			return false;
    	}

    	if(invited != s.invited)
    		return false;

    	if(queries_left != s.queries_left)
    		return false;

    	if(time_index != s.time_index)
    		return false;

    	return true;
    }

    bool operator!=(const state_t &s) const {
        return !(*this==(s));   
    }

    bool operator<(const state_t &s) const {
    	if(time_index < s.time_index)
    		return true;
    	if(time_index > s.time_index)
    		return false;

    	if(queries_left < s.queries_left)
    		return false;
    	if(queries_left > s.queries_left)
    		return true;

    	for(int i = 0; i < NUM_NODES; ++i)
    	{
    		if(presence[i] < s.presence[i])
    			return true;
    		if(presence[i] > s.presence[i])
    			return false;
    	}

    	if(invited.size() < s.invited.size())
    		return true;
    	if(invited.size() > s.invited.size())
    		return false;

    	std::set<int>::iterator it1 = invited.begin();
    	std::set<int>::iterator it2 = s.invited.begin();
    	while(it1 != invited.end())
    	{
    		if(*it1 < *it2)
    			return true;
    		if(*it1 > *it2)
    			return false;
    		++it1;
    		++it2;
    	}

    	return false;
    }

    // Helper function to extract the i^th bit of bitVector
    inline bool get(unsigned short bitVector, int i) const
    {
    	return ((bitVector & (1 << i)) != 0);
    }

     void print(std::ostream &os) const {
     	os << "(";

     	os << "[";
     	for(int i = 0; i < NUM_NODES; ++i)
     		os << presence[i] << ",";
     	os << "]";

     	os << ",";

     	os << "{";
     	for(std::set<int>::iterator it = invited.begin(); it != invited.end(); ++it)
     		os << *it << ",";
     	os << "}";

     	os << ",";
     	os << queries_left;
     	os << ",";
     	os << time_index;
     	os << ")";
    }
    friend class problem_t;
};

inline std::ostream& operator<<(std::ostream &os, const state_t &s) {
    s.print(os);
    return os;
}

class problem_t : public Problem::problem_t<state_t> {
   
    // bool prog_req[NUM_COURSES];
    state_t init_;
    // state_t goal_;
    float show_up_prob[NUM_NODES];
    int rewards[NUM_NODES];

    static const float default_show_up_prob[NUM_NODES];
    static const float default_rewards[NUM_NODES];
    // bool prereq[NUM_COURSES*NUM_COURSES];
    // static const bool default_prereq[NUM_COURSES*NUM_COURSES];
    // static const bool default_prog_req[NUM_COURSES];

  public:
    problem_t(int dim1, int dim2)
      : Problem::problem_t<state_t>(DISCOUNT)
        {
        	init_.presence = new short[NUM_NODES];
        	for(int i = 0; i < NUM_NODES; ++i)
        		init_.presence[i] = -1;

        	init_.invited.clear();
        	init_.queries_left = MAX_QUERIES;
        	init_.time_index = 1;

            // bcopy(default_prereq, prereq, NUM_COURSES*NUM_COURSES * sizeof(bool));
            // bcopy(default_prog_req,prog_req,NUM_COURSES* sizeof(bool));
            bcopy(default_show_up_prob, show_up_prob, NUM_NODES * sizeof(float));
            bcopy(default_rewards, rewards, NUM_NODES * sizeof(float));
            
            // goal_.course_passed_ = ((1 << NUM_COURSES) - 1);
            // goal_.course_taken_ = ((1 << NUM_COURSES) - 1);

    }
    virtual ~problem_t() { }

    virtual Problem::action_t number_actions(const state_t &s) const { return (1 << (NUM_NODES+1)); }

    virtual const state_t& init() const { return init_; }
    virtual bool terminal(const state_t &s) const {
        if(s.time_index == MAX_TIME+1)
        	return true;
        return false;
    }
    virtual bool dead_end(const state_t &s) const { return false; }
    virtual bool applicable(const state_t &s, ::Problem::action_t a) const {
    	bool act_type = s.get(a, NUM_NODES);	// '0' indicates query action & '1' indicates invite action
    	if(act_type == 0)	// Query action
    	{
    		if(s.queries_left == 0)
    			return false;

    		unsigned num = 0;	// Number of nodes queried

    		for(int i = 0; i < NUM_NODES; ++i)
    		{
    			if(s.get(a,i))	// If 'i' is queried in the action
    			{
    				if(s.presence[i] != -1)
    					return false;

    				if(s.invited.find(i) != s.invited.end())	// 'i' is already invited
    					return false;

    				num += 1;
    			}
    		}

    		if(num > MAX_PER_QUERY)
    			return false;

    		return true;
    	}

    	else	// Invite action
    	{
    		unsigned num = 0;	// Number of nodes invited
    		for(int i = 0; i < NUM_NODES; ++i)
    		{
    			if(s.get(a,i))	// if 'i' is being invited in action
    			{
    				if(s.presence[i] != 1)	// But i's presence is not confirmed
    					return false;

    				if(s.invited.find(i) != s.invited.end())	// 'i' is already invited
    					return false;

    				num += 1;
    			}
    		}

    		if(s.invited.size() + num > MAX_INVITE)
    			return false;

    		return true;
    	}
    }


    virtual float cost(const state_t &s, Problem::action_t a) const {
    	// Assuming that a is applicable from s
    	// We get reward ONLY on reaching the goal, i.e. taking invite action from last time index
    	bool act_type = s.get(a, NUM_NODES);
    	if(act_type == 0)	// Query action
    		return 0;

    	if(s.time_index < MAX_TIME)	// This is not the last action
    		return 0;

    	// Get the reward of s.invited UNION action nodes
    	float reward = 0;

     	for(std::set<int>::iterator it = s.invited.begin(); it != s.invited.end(); ++it)
			reward += rewards[*it];
		for(int i = 0; i < NUM_NODES; ++i)
		{
			if(s.get(a,i))
				reward += rewards[i];
		}

		return (-reward);
    }
  

    // virtual float trans_prob(const state_t &s1, Problem::action_t a, const state_t &s2) const
    // {
    // 	// May not be required
    // }

    virtual void sample_factored(const state_t &s, Problem::action_t a, state_t &outcome) const 
    {
     	bool act_type = s.get(a, NUM_NODES);

     	if(act_type == 0)	// Query action
     	{
	    	outcome.presence = new short[NUM_NODES];
	     	for(int i = 0; i < NUM_NODES; i++)
	     	{
	     		if(s.get(a,i))	// if 'i' is queried
	     		{
	     			float p = show_up_prob[i];	// probability that 'i' has shown up
	     			float r = Random::real();

	     			if(r < p)	// 'i' has shown up
	     				outcome.presence[i] = 1;
	     			else
	     				outcome.presence[i] = 0;
	     		}
	     		else	// else presence remains the same
		     		outcome.presence[i] = s.presence[i];
	     	}

	     	outcome.invited = s.invited;
	     	outcome.queries_left = s.queries_left - 1;
 	     	outcome.time_index = s.time_index;

     		assert(outcome.queries_left >= 0);
     	}

     	else	// Invite action, i.e. invite & go on to new session
     	{
	    	outcome.presence = new short[NUM_NODES];	// Clear-up presence
	     	for(int i = 0; i < NUM_NODES; i++)
	     		outcome.presence[i] = -1;

	     	outcome.invited = s.invited;	// Copy previously invited
	     	for(int i = 0; i < NUM_NODES; i++)	// Add newly invited
	     	{
	     		if(s.get(a,i))
	     			outcome.invited.insert(i);
	     	}

	     	outcome.queries_left = MAX_QUERIES;	// Refresh number of queries
	     	outcome.time_index = s.time_index+1;	// Increment session
     	}
    }


    virtual void next(const state_t &s, Problem::action_t a, std::vector<std::pair<state_t,float> > &outcomes) const {
       
         
    }
    virtual void print(std::ostream &os) const { }
};

const float problem_t::default_show_up_prob[] = {
	0.20, 0.03, 0.66, 0.61, 0.83, 0.13, 0.56, 0.11, 0.74, 0.84
};

const float problem_t::default_rewards[] = {
	132.85, 172.55, 142.96, 178.90, 71.71, 108.98, 73.25, 52.62, 164.87, 81.93
};

inline std::ostream& operator<<(std::ostream &os, const problem_t &p) {
    p.print(os);
    return os;
}

class scaled_heuristic_t : public Heuristic::heuristic_t<state_t> {
    const Heuristic::heuristic_t<state_t> *h_;
    float multiplier_;
  public:
    scaled_heuristic_t(const Heuristic::heuristic_t<state_t> *h, float multiplier = 1.0)
      : h_(h), multiplier_(multiplier) { }
    virtual ~scaled_heuristic_t() { }
    virtual float value(const state_t &s) const {
        return h_->value(s) * multiplier_;
    }
    virtual void reset_stats() const { }
    virtual float setup_time() const { return 0; }
    virtual float eval_time() const { return 0; }
    virtual size_t size() const { return 0; }
    virtual void dump(std::ostream &os) const { }
    float operator()(const state_t &s) const { return value(s); }
};

class zero_heuristic_t: public Heuristic::heuristic_t<state_t> {
  public:
    zero_heuristic_t() { }
    virtual ~zero_heuristic_t() { }
    virtual float value(const state_t &s) const { return 0; }
    virtual void reset_stats() const { }
    virtual float setup_time() const { return 0; }
    virtual float eval_time() const { return 0; }
    virtual size_t size() const { return 0; }
    virtual void dump(std::ostream &os) const { }
    float operator()(const state_t &s) const { return value(s); }
};


